package org.frknkrc44.superractocr;

import android.content.ContentResolver;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class DataLoadTask extends AsyncTask<Object, Void, Void> {
    private OnFinishListener listener;

    @Override
    protected Void doInBackground(Object... objects) {
        try {
            TessBaseAPI api = (TessBaseAPI) objects[0];
            File filesDir = (File) objects[1];
            File dataDir = (File) objects[2];
            String dataFolder = (String) objects[3];
            String language = (String) objects[4];
            AssetManager assets = (AssetManager) objects[5];
            listener = (OnFinishListener) objects[6];
            initApiFiles(dataDir, dataFolder, assets);
            api.init(filesDir.toString(), language);
            return null;
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        listener.onFinish();
    }

    private void initApiFiles(File dataDir, String dataFolder, AssetManager assets) {
        try {
            dataDir.mkdirs();
            String[] items = assets.list(dataFolder);
            List<String> filesList = Arrays.asList(Objects.requireNonNull(dataDir.list()));
            for(String item : items) {
                if(filesList.contains(item)) {
                    continue;
                }
                InputStream is = assets.open(dataFolder + File.separator + item);
                FileOutputStream os = new FileOutputStream(dataDir + File.separator + item);
                byte[] buf = new byte[1024];
                int count;
                while((count = is.read(buf, 0, buf.length)) >= 0) {
                    os.write(buf, 0, count);
                }
            }
        } catch(Throwable t) {
            throw new RuntimeException(t);
        }
    }

    public interface OnFinishListener {
        public void onFinish();
    }
}
