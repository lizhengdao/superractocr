package org.frknkrc44.superractocr;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity implements ImageProcessTask.OnFinishListener, DataLoadTask.OnFinishListener {

    private final int RESULT_LOAD_IMAGE = 1;
    private TextView pathView, processView;
    private ImageView imageView;
    private Button selectButton, processButton;
    private Uri photoUri;
    private TessBaseAPI api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        pathView = getTextView(android.R.id.text1);
        processView = getTextView(android.R.id.text2);
        imageView = findViewById(android.R.id.icon);
        selectButton = findViewById(android.R.id.button1);
        processButton = findViewById(android.R.id.button2);
        initApi();
    }

    private void initApi() {
        setButtonsState(false);
        api = new TessBaseAPI();
        String dataFolder = "tessdata";
        File dataFile = new File(getFilesDir() + File.separator + dataFolder);
        String language = "tur+eng";
        pathView.setText(R.string.init_api);
        new DataLoadTask().execute(api, getFilesDir(), dataFile, dataFolder, language, getAssets(), this);
    }

    private TextView getTextView(int resId) {
        return (TextView) findViewById(resId);
    }

    public void onFileSelectButtonClick(View view) {
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i,""), RESULT_LOAD_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri photoData = data.getData();
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != photoData) {
            photoUri = photoData;
            setButtonsState(true);
            try {
                imageView.setImageBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(), photoUri));
            } catch(Throwable t) {
                // do nothing
            }
            pathView.setText(photoData.getPath());
        }
    }

    public void onProcessButtonClick(View view) {
        if(photoUri == null) {
            Toast.makeText(this, R.string.photo_empty, Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            setButtonsState(false);
            new ImageProcessTask().execute(api, getContentResolver(), photoUri, this);
        } catch(Throwable t) {
            throw new RuntimeException(t);
        }
    }

    private void setButtonsState(boolean enabled) {
        selectButton.setEnabled(enabled);
        processButton.setEnabled(enabled);
    }

    @Override
    public void onFinish(String text) {
        setButtonsState(true);
        processView.setText(text);
    }

    @Override
    public void onFinish() {
        setButtonsState(true);
        pathView.setText(R.string.api_ready);
    }
}