package org.frknkrc44.superractocr;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;

import com.googlecode.tesseract.android.TessBaseAPI;

public class ImageProcessTask extends AsyncTask<Object, Void, String> {
    private OnFinishListener listener;

    @Override
    protected String doInBackground(Object... objects) {
        try {
            TessBaseAPI api = (TessBaseAPI) objects[0];
            ContentResolver resolver = (ContentResolver) objects[1];
            Uri imagePath = (Uri) objects[2];
            listener = (OnFinishListener) objects[3];
            Bitmap bmp = MediaStore.Images.Media.getBitmap(resolver, imagePath);
            api.setImage(bmp);
            return api.getUTF8Text();
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        listener.onFinish(s);
    }

    public interface OnFinishListener {
        public void onFinish(String text);
    }
}
